import React from 'react';
import logo from './logo.svg';
import './App.css';
import './feather.css'
import { RoutingModule } from './app-routing-module';

function App() {
  return (
    <div className="App">
      <RoutingModule></RoutingModule>
    </div>
  );
}

export default App;
