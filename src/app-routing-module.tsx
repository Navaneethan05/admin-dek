import { render } from "@testing-library/react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { AdvanceTable } from "./components/advance-table/advance-table";

export const RoutingModule: React.FC = () => {

    const Routes = (<Router>
        <Switch>
            <Route path="/advanced-table" component={AdvanceTable} exact></Route>
        </Switch>
    </Router>)

    return (
        <div id="app-route">
            {Routes}
        </div>
    )

}