import React from "react"
import { AppHeader } from "../app-header/app-header"
import { SideNav } from "../side-nav/side-nav"
import './advance-table.css'

export interface IDataList {
    id: number;
    name: string;
    mobile: string;
    city: string;
    state: string;
    status: boolean;
}

export const AdvanceTable: React.FC = () => {
    // const [dataList, setDataList] = React.useState<IDataList[]>([]);
    const [itemsPerPage, setItemsPerPage] = React.useState<number>(10);
    const [selectedPage, setSelectedPage] = React.useState<number>(1);
    const [currentData, setCurrentData] = React.useState<IDataList[]>([]);
    const [maxPageNumber, setMaxPage] = React.useState<number>(0);
    const [fromIndex, setFromIndex] = React.useState<number>(0);
    const [toIndex, setToIndex] = React.useState<number>(0);
    const [searchText, setSearchText] = React.useState<string>("");
    let delayTimer: any;
    React.useEffect(() => {
        fetch("./data.json", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }).then((response) =>
            response.json()
        ).then((data) => {
            let maxPage = data.length / itemsPerPage;
            let startIndex = maxPage > 1 && selectedPage > 1 ? (selectedPage - 1) * itemsPerPage : 0;
            let endIndex = maxPage > 1 && selectedPage > 1 ? selectedPage * itemsPerPage : itemsPerPage;
            // setDataList(data);
            setFromIndex(startIndex);
            setToIndex(endIndex);
            setCurrentData(data.slice(startIndex, endIndex));
            setMaxPage(maxPage);
            if (searchText) {
                filterData(data.slice(startIndex, endIndex));
            }
        }).catch((error) => console.log(error));
    }, [itemsPerPage, selectedPage, searchText])

    const SaveItemsPerPage = (e: any) => {
        setItemsPerPage(parseInt(e.target.value));
        setSelectedPage(1);
    }

    const filterData = (data: IDataList[]) => {
        let filteredData: IDataList[] = [];

        data.map((data) => {
            if (data.name?.toLowerCase().indexOf(searchText) >= 0 || data.city?.toLowerCase().indexOf(searchText) >= 0 || data.state?.toLowerCase().indexOf(searchText) >= 0) {
                filteredData.push(data);
            }
        });

        setCurrentData(filteredData);
    }

    const setPage = (pageNumber: number) => {
        if (pageNumber > 0 && pageNumber <= maxPageNumber)
            setSelectedPage(pageNumber);
    }

    const searchItems = (e: any) => {
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function () {
            setSearchText(e.target.value?.toString().toLowerCase());
        }, 1000);
    }

    return (
        <>
            <AppHeader></AppHeader>
            <div className="table-container">
                <SideNav></SideNav>
                <div className="table-content">
                    <div className="header-container">
                        <div className="header">Farmer Details</div>
                        <div className="nav-item">
                            <i className="feather icon-home" style={{ marginRight: "10px" }}></i>
                            <span>Dashboard / Farmer Details</span>
                        </div>
                    </div>
                    <div className="content">
                        <div className="action-items">
                            <div style={{ display: "flex" }}>
                                <div>
                                    <select className="items-per-page" name="itemPerPage" id="count" value={itemsPerPage} onChange={SaveItemsPerPage}>
                                        <option>10</option>
                                        <option>25</option>
                                        <option>50</option>
                                        <option>100</option>
                                    </select>
                                </div>
                                <div className="btn-primary">
                                    <span>Action</span>
                                    <i className="feather icon-chevron-down" />
                                </div>
                                <div className="btn-primary">
                                    <span>Export</span>
                                    <i className="feather icon-chevron-down" />
                                </div><
                                    div className="btn-primary">
                                    <span>Filter</span>
                                </div>
                            </div>
                            <div className="search">
                                <input type="text" placeholder="Search..." onChange={(e) => searchItems(e)} />
                                <span><i className="feather icon-search" /></span>
                            </div>
                        </div>
                        <div className="table">
                            <table>
                                <tr>
                                    <th className="id">#</th>
                                    <th className="name">Name</th>
                                    <th className="mobile">Mobile</th>
                                    <th className="city">City</th>
                                    <th className="state">State</th>
                                    <th className="status">Status</th>
                                    <th className="action">Action</th>
                                </tr>
                                {
                                    currentData.map((d, i) =>
                                        <tr key={i}>
                                            <td className="id">{d.id}</td>
                                            <td className="name">{d.name}</td>
                                            <td className="mobile">{d.mobile}</td>
                                            <td className="city">{d.city}</td>
                                            <td className="state">{d.state}</td>
                                            <td className="status">
                                                {d.status ? <i className="feather icon-check" style={{
                                                    color: "green",
                                                    fontWeight: "bold"
                                                }}
                                                /> :
                                                    <i className="feather icon-x" style={{
                                                        color: "red",
                                                        fontWeight: "bold"
                                                    }}
                                                    />
                                                }
                                            </td>
                                            <td className="action">
                                                <span><i className="feather icon-file-text" /></span>
                                                <span><i className="feather icon-edit" /></span>
                                                <span><i className="feather icon-delete" /></span>
                                            </td>
                                        </tr>
                                    )
                                }
                            </table>
                            <div className="table-footer">
                                <div className="desc">Showing {fromIndex + 1} to {toIndex} of {itemsPerPage} Items</div>
                                {
                                    maxPageNumber > 1 && <div className={"table-nav"}>
                                        <div onClick={() => setPage(selectedPage - 1)}> Previous</div>

                                        <div className={selectedPage == 1 ? "page-active" : ""} onClick={() => setPage(selectedPage == 1 ? selectedPage : maxPageNumber > 2 && selectedPage === maxPageNumber ? selectedPage - 2 : selectedPage - 1)}>{selectedPage == 1 ? selectedPage : maxPageNumber > 2 && selectedPage === maxPageNumber ? selectedPage - 2 : selectedPage - 1}</div>

                                        {maxPageNumber >= 2 &&
                                            <div className={selectedPage > 1 && (selectedPage < maxPageNumber || maxPageNumber <= 2) ? "page-active" : ""} onClick={() => setPage(selectedPage == 1 ? selectedPage + 1 : (maxPageNumber > 2 && selectedPage === maxPageNumber) ? selectedPage - 1 : selectedPage)}> {selectedPage == 1 ? selectedPage + 1 : (maxPageNumber > 2 && selectedPage === maxPageNumber) ? selectedPage - 1 : selectedPage}</div>
                                        }

                                        {maxPageNumber > 3 &&
                                            <>
                                                <div style={{ cursor: "auto" }}>...</div>

                                                <div className={selectedPage == maxPageNumber ? "page-active" : ""} onClick={() => setPage(maxPageNumber)}>{maxPageNumber}</div>
                                            </>
                                        }

                                        <div onClick={() => setPage(selectedPage + 1)}>Next</div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}