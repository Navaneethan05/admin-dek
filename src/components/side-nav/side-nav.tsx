import React from "react"
import './side-nav.css'
export const SideNav: React.FC = () => {
    return (
        <div className="side-nav-container">
            <div className="nav-items">
                <span>
                    <i className="feather icon-home" />
                </span>
                <span className="content">Dashboard</span>
                <span className="icon"><i className="feather icon-chevron-right" /></span>
            </div>
            <div className="nav-items">
                <span>
                    <i className="feather icon-list" />
                </span>
                <span className="content">Activity</span>
                <span className="icon"><i className="feather icon-chevron-right" /></span>
            </div>
            <div className="nav-items">
                <span>
                    <i className="feather icon-users" />
                </span>
                <span className="content">Farmer</span>
                <span className="icon"><i className="feather icon-chevron-right" /></span>
            </div>
            <div className="nav-items">
                <span>
                    <i className="feather icon-calendar" />
                </span>
                <span className="content">Schedule Template</span>
                <span className="icon"><i className="feather icon-chevron-right" /></span>
            </div>
        </div>
    )
}