import React from 'react';
import logo from '../../assets/png/logo.png'
import user from '../../assets/jpg/avatar-4.jpg'
import './app-header.css';

export const AppHeader: React.FC = () => {
    return (
        <div className="app-header">
            <div className="logo">
                <img src={logo} style={{ padding: "23px 0" }} />
                <span className="toggle"><i className="feather icon-menu icon-toggle-left" /></span>
            </div>
            <div className="title">FarmRight</div>
            <div className="widget">
                <span className="notification"><i className="feather icon-bell" /></span>
                <span className="message"><i className="feather icon-message-square" /></span>
                <img src={user} width="40" height="40" style={{ borderRadius: "50%", marginRight:"15px" }} />
                <span className="userName">John Doe</span>
                <span><i className="feather icon-chevron-down" /></span>
            </div>
        </div>
    )
}